package Test.quarts;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.KeyMatcher;

import com.demo.job.PrintTask;
import com.demo.job.jobListener.MyJobListener;

public class Test1 {

    public static void main(String[] args) {
        // 创建一个JobDetail对象 同时设置任务名称和任务组名
        JobKey jobKey = new JobKey("dummyJobName", "group1");
        JobDetail job = JobBuilder.newJob(PrintTask.class).withIdentity(jobKey).build();

        // 触发器 设置触发器的名称和组名称 设置触发器的cron规则
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName", "group1")
                // cron表达式 每五秒钟执行一次
                .withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?")).build();

        // 创建调度器 通过标准调度工厂配置调度器
        Scheduler scheduler = null;
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.getListenerManager().addJobListener(new MyJobListener(), KeyMatcher.keyEquals(jobKey));

            // 启动调度器
            scheduler.start();

            // 设置调度器的job详情和触发器
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

}
