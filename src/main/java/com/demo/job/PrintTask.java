package com.demo.job;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author 梁海鹏
 * @date 2017年6月13日 下午1:46:29
 * @class com.demo.job.PrintTask
 *
 *
 */
public class PrintTask implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        Date nextFireTime = context.getNextFireTime();

        System.out.println("》》》》》》》》》》》》这是执行quartz的任务类" + nextFireTime + "》》》》》》》》》》》》》》》》》");
    }

}
