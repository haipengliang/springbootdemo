package com.demo.bean;

/**
 *
 * @author 梁海鹏
 * @date 2017年6月12日 下午4:30:36
 * @class com.demo.bean.Book
 *
 *
 */
public class Book {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Book [name123=" + name + ", value=" + value + "]";
    }

}
