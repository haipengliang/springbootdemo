package com.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.demo.bean.Book;

@Mapper
public interface BookMapper {

    public List<Book> findBook();

    int insert(@Param("book") Book book);
}
