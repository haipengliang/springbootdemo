package com.demo.service;

import com.demo.bean.Book;

public interface BookService {

    public int addBook(Book book);

    public void deleteBook(String name);

    public void queryBook();

}
