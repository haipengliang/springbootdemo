package com.demo.service.impl;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.demo.bean.Book;
import com.demo.service.BookService;

@Service
public class BookServiceImpl implements BookService {

    @Resource
    JdbcTemplate template;

    @Override
    public int addBook(Book book) {
        // TODO Auto-generated method stub
        int update = template.update(
                "INSERT INTO `book` (`name`, `value`) VALUES ('" + book.getName() + "', '" + book.getValue() + "')");
        return update;
    }

    @Override
    public void deleteBook(String name) {
        // TODO Auto-generated method stub

    }

    @Override
    public void queryBook() {
        // TODO Auto-generated method stub
        Book book = template.queryForObject("select * from book", Book.class);
        System.out.println(book);
    }

}
