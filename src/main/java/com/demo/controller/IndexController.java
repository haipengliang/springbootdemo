package com.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.demo.bean.Book;
import com.demo.mapper.BookMapper;
import com.demo.service.BookService;

@Controller
public class IndexController {

    @Autowired
    private BookService bookService;

    @Autowired
    private BookMapper book;

    @RequestMapping("/add")
    public void add() {
        Book b = new Book();
        b.setName("sql");
        b.setValue("demo");
        bookService.addBook(b);
    }

    @RequestMapping("/addBook")
    public void addBook() {
        Book b = new Book();
        b.setName("mybatis");
        b.setValue("mapper");

        book.insert(b);
    }

    @RequestMapping(value = "/query", method = RequestMethod.GET)
    @ResponseBody
    public String query() {
        List<Book> findBook = book.findBook();
        findBook.forEach(c -> System.out.println(c));
        String jsonString = JSON.toJSONString(findBook);
        return jsonString;
    }

    @RequestMapping("/setTest")
    public void setText(HttpServletRequest request, HttpServletResponse response) {
        String parameter = request.getParameter("name");
        System.out.println(parameter);
    }
}
