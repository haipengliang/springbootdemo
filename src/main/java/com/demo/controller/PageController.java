package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PageController {

    @RequestMapping("/demo")
    @ResponseBody
    public String demo() {
        return "您已成功部署项目";
    }

    @RequestMapping("/index")
    public String hello(ModelMap modelMap) {
        // 向模板中添加属性
        modelMap.put("hello", "helloweb");
        // return模板文件的名称，对应src/main/resources/templates/index.html
        return "index";
    }
}