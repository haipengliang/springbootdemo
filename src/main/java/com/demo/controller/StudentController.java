package com.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.bean.Student;

@RestController
@RequestMapping("/stu")
public class StudentController {

    private static Map<Integer, Student> map = new HashMap<>();

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String add(@ModelAttribute Student stu) {
        System.out.println(stu);
        map.put(stu.hashCode(), stu);
        return "success";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Student> query() {
        List<Student> list = new ArrayList<>(map.values());
        return list;
    }

}
